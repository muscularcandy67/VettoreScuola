package com.muscularcandy67;

public class Main {

    public static void main(String[] args) {
	    Vettore vett = new Vettore(1);
	    vett.aggiungiElementoInCoda(3);
	    vett.aggiungiElementoInCoda(6.2);
	    vett.aggiungiElementoInTesta("Ciao");
	    System.out.println(vett.getVettoreByNumber(0));
	    System.out.println(vett.getVettoreByNumber(1));
	    System.out.println(vett.getVettoreByNumber(2));
    }
}
