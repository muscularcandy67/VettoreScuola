•	Es “vettore1”. Creare una classe Vettore che contenga al su interno tutti metodi per la gestione di un vettore. Il costruttore provvederà alla creazione del vettore con dimensione fissa = costante max. Nella classe saranno presenti i metodi per aggiungere un elemento in coda, aggiungere un elemento in testa, aggiungere un elemento in una certa posizione, eliminare tutti gli elementi.

•	Es “vettore2”. Aggiungere alla classe precedente i metodi per l’ordinamento in modo crescente, e in modo decrescente.

•	Es “vettore3”. Modificare l’esercizio “vettore1” in modo che le dimensioni del vettore siano modificabili in modo dinamico in base al numero di elementi presenti.
